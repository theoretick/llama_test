
## Running tests

```bash
# Spins up test server and executes both success and failure test cases against it
./scripts/test.sh
```

