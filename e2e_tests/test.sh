#!/usr/bin/env ruby

puts "=> Starting API"
pipe = IO.popen("cd ./CustomerService && dotnet run")
sleep 2

puts "=> Running Pact"
puts "=> Test FAILING Pact"
res = `pact-provider-verifier --provider-base-url http://localhost:5000 --pact-urls ./e2e_tests/fail.json`
puts res

puts "=> Test SUCCESSFUL Pact"
res = `pact-provider-verifier --provider-base-url http://localhost:5000 --pact-urls ./e2e_tests/success.json`
puts res

puts "=> Shutting down API"
Process.kill 'USR2', pipe.pid

puts "Test exit status: #{res}"
puts
